pub trait Collide {
    fn collide(&mut self, &mut Self);
}

pub trait Update {
    fn update(&mut self);
}



