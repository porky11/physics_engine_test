extern crate num_traits;
//extern crate vector_space;
#[macro_use]
extern crate derive_deref;

mod traits;
mod generate;
mod spatial;

//mod shape;
//mod colliding;
//When I have trait `A`, that implements method `x(self)` of Trait `X`, and have type B, which implements `Deref<Target=A>`, can I call `fn<T: X>(
#[cfg(test)]
mod tests {
    #[test]
    fn update() {
        use generate::{With,Simple};
        use spatial::{
            Spatial,
            Expose,
            Accelerate,
        };
        
        let mut pos = Simple.with(0i8).dynamic();
        //let mut vel = Simple.with(0).expose_position().dynamic().with(0).dynamic();
        //let mut acc = Simple.with(0).expose_position().dynamic().with(0).dynamic().with(0).dynamic();
        /*

        pos.accelerate(1);
        vel.accelerate(1);
        acc.accelerate(1);
        
        assert_eq!(pos.pos(), 1);
        assert_eq!(vel.pos(), 0);
        assert_eq!(acc.pos(), 0);
        
        pos.update();
        vel.update();
        acc.update();

        assert_eq!(pos.pos(), 1);
        assert_eq!(vel.pos(), 1);
        assert_eq!(acc.pos(), 1);
        
        pos.update();
        vel.update();
        acc.update();

        assert_eq!(pos.pos(), 1);
        assert_eq!(vel.pos(), 2);
        assert_eq!(acc.pos(), 3);*/
    }

}

