use std::ops::{Deref,DerefMut};

pub trait ExposeValue {
    type Exposed;
    fn value(&self) -> &Self::Exposed;
    fn value_mut(&mut self) -> &mut Self::Exposed;
}

#[derive(Copy,Clone,Debug)]
pub struct Simple;

#[derive(Copy,Clone,Debug)]
pub struct Value<I,T> {
    inner: I,
    value: T
}

impl<I,T> Deref for Value<I,T> {
    type Target = I;
    #[inline]
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<I,T> DerefMut for Value<I,T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl<I,T> ExposeValue for Value<I,T> {
    type Exposed = T;
    #[inline]
    fn value(&self) -> &Self::Exposed {&self.value}
    #[inline]
    fn value_mut(&mut self) -> &mut Self::Exposed {&mut self.value}
}

pub trait With<T: ExposeValue>: Sized {
    fn with(self, T::Exposed) -> T;
}

impl<I,T> With<Value<I,T>> for I where I: Sized {
    #[inline]
    fn with(self, value: T) -> Value<I,T> {
        Value {
            inner: self,
            value
        }
    }
}

