use traits::Update;

use num_traits::Zero;

use std::ops::{Deref,DerefMut};

pub trait Accelerate<T> {
    fn accelerate(&mut self, T);
}

pub trait Transform<T> {
    fn transform(&mut self, T);
}

use std::ops::AddAssign;

impl<T: AddAssign> Transform<T> for T {
    fn transform(&mut self, inc: T) {
        *self += inc
    }
}

pub trait ExposePosition {
    type Type;
    fn pos(&self) -> Self::Type;
}

pub trait ExposeVelocity {
    type Type;
    fn vel(&self) -> Self::Type;
}

pub trait ExposeOrientation {
    type Type;
    fn dir(&self) -> Self::Type;
}

pub trait ExposeRotation {
    type Type;
    fn rot(&self) -> Self::Type;
}

use generate::{
    ExposeValue,
    Simple,
    Value
};


impl<T> Accelerate<T> for Simple {
    #[inline]
    fn accelerate(&mut self, _: T) {}
}

impl Update for Simple {
    #[inline]
    fn update(&mut self) {}
}


#[derive(Copy,Clone,Deref,DerefMut)]
pub struct Position<I> where I: ExposeValue {
    inner: I
}

impl<I> ExposePosition for Position<I> where I: ExposeValue, <I as ExposeValue>::Exposed: Copy {
    type Type = I::Exposed;
    fn pos(&self) -> Self::Type {
        *self.value()
    }
}


#[derive(Copy,Clone,Deref,DerefMut)]
pub struct Velocity<I> where I: ExposeValue {
    inner: I
}

impl<I> ExposeVelocity for Velocity<I> where I: ExposeValue, <I as ExposeValue>::Exposed: Copy {
    type Type = I::Exposed;
    fn vel(&self) -> Self::Type {
        *self.value()
    }
}


#[derive(Copy,Clone,Deref,DerefMut)]
pub struct Orientation<I> where I: ExposeValue {
    inner: I
}

impl<I> ExposeOrientation for Orientation<I> where I: ExposeValue, <I as ExposeValue>::Exposed: Copy {
    type Type = I::Exposed;
    fn dir(&self) -> Self::Type {
        *self.value()
    }
}


#[derive(Copy,Clone,Deref,DerefMut)]
pub struct Rotation<I> where I: ExposeValue {
    inner: I
}

impl<I> ExposeRotation for Rotation<I> where I: ExposeValue, <I as ExposeValue>::Exposed: Copy {
    type Type = I::Exposed;
    fn rot(&self) -> Self::Type {
        *self.value()
    }
}






#[derive(Copy,Clone,Deref,DerefMut)]
pub struct Dynamic<I> where I: ExposeValue+Accelerate<<I as ExposeValue>::Exposed> {
    inner: I,
}

impl<I,T> Accelerate<T> for Dynamic<I>
    where I: ExposeValue+Accelerate<<I as ExposeValue>::Exposed>,
          <I as ExposeValue>::Exposed: Transform<T>,
          T: Copy {
    #[inline]
    fn accelerate(&mut self, acc: T) {
        self.value_mut().transform(acc);
    }
}

impl<I> Update for Dynamic<I>
    where I: ExposeValue+Accelerate<<I as ExposeValue>::Exposed>+Update,
          <I as ExposeValue>::Exposed: Copy {
    #[inline]
    fn update(&mut self) {
        let val = *self.value();
        self.inner.accelerate(val);
        self.inner.update();
    }
}

pub trait Expose: ExposeValue+Sized {
    fn expose_position(self) -> Position<Self>;
    //fn expose_velocity(self) -> Velocity<Self>;
    //fn expose_orientation(self) -> Orientation<Self>;
    //fn expose_rotation(self) -> Rotation<Self>;
}

impl<I> Expose for I where I: ExposeValue {
    fn expose_position(self) -> Position<Self> {
        Position{inner: self}
    }
}

pub trait Spatial: ExposeValue+Sized {
    fn dynamic(self) -> Dynamic<Self>
        where Self: Accelerate<<Self as ExposeValue>::Exposed>;
    //fn delayed<T: Sized>(self, vel: T) -> Delayed<Self,T> where Self: Accelerate<T>;
    //fn mix<T>(self, rot: T) -> Mixed<Self,T>;
}



impl<I> Spatial for I where I: ExposeValue {
    fn dynamic(self) -> Dynamic<Self>
            where Self: Accelerate<<Self as ExposeValue>::Exposed> {
        Dynamic {
            inner: self
        }
    }/*
    fn delayed<T>(self, acc: T) -> Delayed<Self,T> where Self: Accelerate<T>{
        Delayed {
            inner: self,
            acc
        }
    }
    fn mix<T>(self, rot: T) -> Mixed<Self,T> {
        Mixed {
            pos: self,
            rot
        }
    }*/
}


/*

#[derive(Copy,Clone)]
pub struct Delayed<P,T> where P: Accelerate<T> {
    inner: P,
    acc: T,
}

impl<P,T,U> Accelerate<U> for Delayed<P,T> where P: Accelerate<T>, T: Transform<U>, U: Copy {
    #[inline]
    fn accelerate(&mut self, acc: U) {
        self.acc.transform(acc);
    }
}

impl<P,T> Update for Delayed<P,T> where P: Accelerate<T>+Update, T: Copy+Zero {
    #[inline]
    fn update(&mut self) {
        self.inner.accelerate(self.acc);
        self.acc = T::zero();
        self.inner.update();
    }
}

impl<P,T> Position for Delayed<P,T> where P: Position+Accelerate<T> {
    type Type = P::Type;
    fn pos(&self) -> Self::Type {
        self.inner.pos()
    }
}

pub struct Mixed<T,U> {
    pos: T,
    rot: U
}
*/
/*

*/
/*



pub struct DelayedMoving<T> {
    pub pos: T,
    pub vel: T,
    pub acc: T,
}





impl<T> Default for DelayedMovingRotating<T> where T: Float {
    fn default() -> Self {
        Self {
            pos: Vector::zero(),
            vel: Vector::zero(),
            acc: Vector::zero(),

            dir: Rotor::one(),
            rot: Bivector::zero(),
            boo: Bivector::zero(),
        }
    }
}





pub struct Convex<T> {
    pub pos: Vector<T>,
    pub vel: Vector<T>,
    pub acc: Vector<T>,

    pub dir: Rotor<T>,
    pub rot: Bivector<T>,
    pub boo: Bivector<T>,
    
    pub points: Vec<Vector<T>>,
    #[cfg(feature = "physics_2D")]
    pub indices: Vec<[usize;2]>,

    #[cfg(feature = "physics_3D")]
    pub indices: Vec<[usize;3]>,

    pub col: [f32;4]
}


impl<T> Default for Convex<T> where T: Float {
    fn default() -> Self {
        Self {
            pos: Vector::zero(),
            vel: Vector::zero(),
            acc: Vector::zero(),

            dir: Rotor::one(),
            rot: Bivector::zero(),
            boo: Bivector::zero(),
            
            indices: Vec::new(),
            points: Vec::new(),
            
            col: [1.0,1.0,1.0,1.0]
        }
    }
}
*/

